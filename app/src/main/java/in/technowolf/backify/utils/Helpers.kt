package `in`.technowolf.backify.utils

import `in`.technowolf.backify.model.BackupModel
import `in`.technowolf.backify.model.RestoreModel
import android.content.Context
import android.content.pm.PackageManager
import java.io.File
import java.util.ArrayList

class Helpers {

    fun loadBackupAPK(context: Context): List<RestoreModel> {
        val appList = ArrayList<RestoreModel>()
        val root = File(Constants.BACKUP_FOLDER)
        if (root.exists() && root.isDirectory) {
            for (file in root.listFiles()!!) {
                if (file.length() > 0 && file.path.endsWith(".apk")) {
                    val filePath = file.path
                    val pk = context.packageManager.getPackageArchiveInfo(
                        filePath,
                        PackageManager.GET_ACTIVITIES
                    )
                    if (pk != null) {
                        val info = pk.applicationInfo
                        info.sourceDir = filePath
                        info.publicSourceDir = filePath
                        val icon = info.loadIcon(context.packageManager)
                        val app = RestoreModel(file.name,filePath,file,icon,null)
                        appList.add(app)
                    }
                }
            }
        }
        return appList
    }

    fun backupExistChecker(backups: List<BackupModel>, ctx: Context): List<BackupModel> {
        val root = File(Constants.BACKUP_FOLDER)
        if (root.exists() && root.run { isDirectory }) {
            for (file in root.listFiles()!!) {
                if (file.length() > 0 && file.path.endsWith(".apk")) {
                    for (i in backups.indices) {
                        val name =
                            backups[i].appName + "_" + backups[i].versionName + ".apk"
                        if (file.name == name) {
                            backups[i].exist = true
                            break
                        }
                    }
                }
            }
        }
        return backups
    }
}