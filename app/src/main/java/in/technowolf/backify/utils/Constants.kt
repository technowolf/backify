package `in`.technowolf.backify.utils

import android.os.Environment
import java.io.File

object Constants {
    private val BASE_PATH = Environment.getDataDirectory().toString() + File.separator
    val BACKUP_FOLDER = BASE_PATH + "Backify" + File.separator
}
