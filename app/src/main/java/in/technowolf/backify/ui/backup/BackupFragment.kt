package `in`.technowolf.backify.ui.backup

import `in`.technowolf.backify.R
import `in`.technowolf.backify.databinding.FragmentBackupBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.fragment_backup.*


class BackupFragment : Fragment() {

    private lateinit var backupViewModel: BackupViewModel

    private lateinit var binding: FragmentBackupBinding
    private var backupAdapter = BackupAdapter(listOf())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        backupViewModel =
            ViewModelProviders.of(this).get(BackupViewModel::class.java)
        val rootView = inflater.inflate(R.layout.fragment_backup, container, false)
        binding = FragmentBackupBinding.inflate(inflater)

        backupViewModel.getAppList(activity!!.packageManager)
        attachObserver()

        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupRecyclerView()
    }

    private fun attachObserver() {
        backupViewModel.appListLiveData.observe(this, Observer {
            Logger.d("Backup model $it")
            backupAdapter.updateData(it)
        })
    }

    private fun setupRecyclerView() {
        backupRecyclerView.layoutManager = LinearLayoutManager(context)
        backupRecyclerView.adapter = backupAdapter
    }
}
