package `in`.technowolf.backify.ui.backup

import `in`.technowolf.backify.model.BackupModel
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import java.io.File

class BackupViewModel : ViewModel() {

    val appListLiveData = MediatorLiveData<List<BackupModel>>()

    private val appList: MutableList<BackupModel> = mutableListOf()

    fun getAppList(packageManager: PackageManager, sortAcceding: Boolean = true) {
        try {
            val packageInfoList = packageManager.getInstalledPackages(0)
            for (i in 0 until packageInfoList.size) {
                val packageInfo = packageInfoList[i]
                if (packageInfo.applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM != 0) {
                    continue
                }
                appList.add(
                    BackupModel(
                        packageInfo.applicationInfo.loadLabel(packageManager).toString(),
                        packageInfo.packageName,
                        packageInfo.versionName,
                        packageInfo.versionCode,
                        packageInfo.applicationInfo.loadIcon(packageManager),
                        0,
                        File(packageInfo.applicationInfo.publicSourceDir)
                    )
                )
            }
        } catch (e: Exception) {

        }

        appList.sortWith(
            if (sortAcceding) Comparator { L, R -> L.appName.compareTo(R.appName, true) }
            else Comparator { L, R -> R.appName.compareTo(L.appName, true) }
        )
        appListLiveData.postValue(appList)
    }
}
