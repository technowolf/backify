package `in`.technowolf.backify.ui.backup

import `in`.technowolf.backify.R
import `in`.technowolf.backify.model.BackupModel
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.backup_app_row.view.*

class BackupAdapter(private var backupList: List<BackupModel>) :
    RecyclerView.Adapter<BackupViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BackupViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.backup_app_row, parent, false)
        return BackupViewHolder(itemView)

    }

    override fun getItemCount(): Int = backupList.size

    override fun onBindViewHolder(holder: BackupViewHolder, position: Int) {
        val backupApp = backupList[position]

        holder.itemView.apply {
            this.appIcon.setImageDrawable(backupApp.appIcon)
            this.appName.text = backupApp.appName
            this.appPackageName.text = backupApp.packageName
            //this.appVersion.text = backupApp.versionNumber.toString()
        }
    }

    fun updateData(list: List<BackupModel>) {
        this.backupList = list
        notifyDataSetChanged()
    }
}

class BackupViewHolder(view: View) : RecyclerView.ViewHolder(view)