package `in`.technowolf.backify.ui.restore

import `in`.technowolf.backify.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment

class RestoreFragment : Fragment() {

    private lateinit var restoreViewModel: RestoreViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        restoreViewModel =
            ViewModelProviders.of(this).get(RestoreViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_restore, container, false)
        val textView: TextView = root.findViewById(R.id.text_home)
        restoreViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<View>(R.id.button_home).setOnClickListener {
            /*val action =
                HomeFragmentDirections.actionHomeFragmentToHomeSecondFragment(
                    "From HomeFragment"
                )
            NavHostFragment.findNavController(this@RestoreFragment)
                .navigate(action)*/
        }
    }
}
