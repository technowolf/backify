package `in`.technowolf.backify.model

import android.graphics.drawable.Drawable
import java.io.File

data class RestoreModel(
    var name: String,
    var path: String,
    var file: File,
    var icon: Drawable,
    var appMemory: Long?,
    var checked: Boolean = false
)

data class BackupModel constructor(
    var appName: String,
    var packageName: String,
    var versionName: String,
    var versionNumber: Int,
    var appIcon: Drawable,
    var appMemory: Long,
    var file: File?,
    var checked: Boolean = false,
    var exist: Boolean = false
)
